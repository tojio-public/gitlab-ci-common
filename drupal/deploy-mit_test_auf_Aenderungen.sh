#!/bin/bash

echo "CI: Information: Ausgabe deploy.sh"

if [[ -z $CICD_STAGING_KEY ]]; then
  echo "CI: staging key not supplied, please add the secret key of the remote server as gitlab variable named CICD_STAGING_KEY"
  exit 127;
fi
if [[ -z $CICD_SERVER_USER ]]; then
  echo "CI: server user not supplied, please add the server user name of the remote server as gitlab variable named CICD_SERVER_USER"
  exit 127;
fi
if [[ -z $CICD_SERVER ]]; then
  echo "CI: server not supplied, please add the name of the remote server as gitlab variable named CICD_SERVER"
  exit 127;
fi
if [[ -z $CICD_PROJECT_DIR ]]; then
  echo "CI: project directory not supplied, please add the project directory from the remote server as absolute path with no trailing slash as gitlab variable named CICD_PROJECT_DIR"
  exit 127;
fi

# get variables
SERVER_USER=$CICD_SERVER_USER
SERVER=$CICD_SERVER
# absolute path to the project, no trailing slash
PROJECT_DIR=$CICD_PROJECT_DIR
# environemnt name set in .gitlab-ci.yml: dev or live
PROJECT_NAME=$CI_ENVIRONMENT_NAME

# echo out variables
echo "CI: User@server:  " $SERVER_USER@$SERVER
echo "CI: Project dir:  " $PROJECT_DIR
echo "CI: Project Name: " $PROJECT_NAME

# install and prepare ssh agent
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
mkdir -p ~/.ssh
eval $(ssh-agent -s)
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh-add <(echo "$CICD_STAGING_KEY")

# deployment
echo "CI: start deployment"
echo "CI: create database dump"
echo "\
  echo "CI: checking target directory"
  if [ ! -d $PROJECT_DIR/$PROJECT_NAME/web ]
    echo "Command cd $PROJECT_DIR/$PROJECT_NAME/web failed, no such directory.";
    exit 127;
  end
  echo "CI: target directory $PROJECT_DIR/$PROJECT_NAME/web exists"
  cd $PROJECT_DIR/$PROJECT_NAME/web
  mkdir -p -- "../../dbbackups"
  drush status
  drush sql:dump --gzip --result-file=../../dbbackups/${CI_ENVIRONMENT_NAME}_${CI_JOB_ID}.sql
  mkdir $PROJECT_DIR/_tmp
  if [ "$PROJECT_NAME" = "live" ]
    echo "CI: PRE files/cae"
    ls -lh $PROJECT_DIR/$PROJECT_NAME/files/default/files/cae
    echo "hier die system.site.yml:"
      cat $PROJECT_DIR/$PROJECT_NAME/files/default/files/cae/system.site.yml
  end
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Target directory problems, exiting."
  exit 127
fi

echo "CI: copy files"
rm -rf artifacts
rm -rf files
rm -rf settings
tar cfz - . | ssh $SERVER_USER@$SERVER "cd $PROJECT_DIR/_tmp; tar xzf -"

echo "CI: copy settings"
echo "\
  mv $PROJECT_DIR/$PROJECT_NAME $PROJECT_DIR/old
  mv $PROJECT_DIR/_tmp $PROJECT_DIR/$PROJECT_NAME
  mv $PROJECT_DIR/old/settings $PROJECT_DIR/$PROJECT_NAME/settings
  mv $PROJECT_DIR/old/files $PROJECT_DIR/$PROJECT_NAME/files
  if [ ! -d $PROJECT_DIR/$PROJECT_NAME/drush ]
    mkdir $PROJECT_DIR/$PROJECT_NAME/drush
    echo "Directory $PROJECT_DIR/$PROJECT_NAME/drush created."
  end
  cp $PROJECT_DIR/old/drush/drush.yml $PROJECT_DIR/$PROJECT_NAME/drush/
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "CI: remove directory 'old'"
echo "\
  rm -rf $PROJECT_DIR/old
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Removing directory old failed, exiting."
  echo "IMPORTANT: Before any further action is taken, YOU have to log in to the server and remove the directory old MANUALLY! (see task 23897604)"
  exit 127
fi

echo "CI: update db"
echo "\
  chmod g-w $PROJECT_DIR/$PROJECT_NAME/web/sites/default
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush cr
  drush updb -y
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Update DB with drush failed, exiting."
  exit 127
fi

echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush cim -y
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Configuration import with drush failed, exiting."
  exit 127
fi

echo "CI: import translations, rebuild cache"
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush locale-check
  drush locale-update
  drush cr
  drush config:status
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "CI: apply git repository settings"
echo "CI: origin: git@gitlab.com:"$CI_PROJECT_PATH.git
echo "CI: branch: " $BRANCH
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME
  git remote set-url origin git@gitlab.com:$CI_PROJECT_PATH.git
  git checkout $BRANCH
  git pull
  " | ssh -p22 -T $SERVER_USER@$SERVER

# possibly, there were config changes during merge/build/deployment
# trigger config auto export manually, @see .config/fish/functions/triggercae.fish
# this is called via a fish-function because the command:
# drush php:eval "\Drupal::service('config_auto_export.service')->triggerExport(TRUE);"
# couldnt be wrapped in a fish friendly format to be inserted here directly
# TODO: check, if the drush php:eval command can be executed without the fish-function
# (REMARK: files/default/files/cae ALWAYS(!) contains files, because we execute
# drush cim (see above) with a new config set from the previous build)
if [ "$PROJECT_NAME" == "live" ]; then
  echo "CI: checking, if there were config changes during merge/deployment"
  echo "\
    cd $PROJECT_DIR/$PROJECT_NAME
    if [ -n (not string length -q -- (ls -A files/default/files/cae)) ]
      # cae contains files
      echo "hier die system.site.yml VOR triggercae:"
      cat files/default/files/cae/system.site.yml
      echo "CI: trigger cae webhook for config auto export commits"
      triggercae
      echo "hier die system.site.yml NACH triggercae:"
      cat files/default/files/cae/system.site.yml
      echo "CI: import changed config"
      drush cim -y
    else
      # cae is empty (or does not exist)
      echo "CI: trigger cae webhook not neccessary, files/default/files/cae is empty"
    end
    " | ssh -p22 -T $SERVER_USER@$SERVER
fi

# remove ourself
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME
  rm deploy-test.sh
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "CI: finished deployment"
