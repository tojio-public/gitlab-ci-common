FROM registry.lakedrops.com/docker/gitlab-drupal-ci/php-8.2

RUN wget https://robo.li/robo.phar
RUN chmod +x robo.phar && mv robo.phar /usr/local/bin/robo

RUN apk add --update --no-cache nodejs npm
