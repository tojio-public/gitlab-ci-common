# Dockerfile
Hinweis: image ist abgeleitet von https://gitlab.lakedrops.com/docker/gitlab-drupal-ci/container_registry/18

neues image erstellen:
auf dem devops server (Dockerfile in ~/projects/gitlab-ci-common) folgende Sequenz:
```sh
docker login registry.gitlab.com
docker build --no-cache --pull -t registry.gitlab.com/tojio-public/gitlab-ci-common/gitlab-ci-common-7.4 .
docker push registry.gitlab.com/tojio-public/gitlab-ci-common/gitlab-ci-common-7.4
```

getestet werden kann das neue image mit:
```sh
docker run -ti --name meincontainer registry.gitlab.com/tojio-public/gitlab-ci-common/gitlab-ci-common-7.4:latest fish
```
testen, was auch immer getestet werden soll (zB composer --version) dann mit exit wieder aus dem container raus
```sh
docker rm meincontainer
```

# common ci verwenden

Unter "Settings | General | Merge requests" die Einstellung "Enable 'Delete source branch' option by default" ausschalten.

Variablen im gitlab-Projekt unter Settings | CI/CD | Variables definieren:
- CICD_STAGING_KEY
secret ssh key für ssh-Zugriff auf den remote server, auf dem das Projekt deployed wird (hier kann ein secret key verwendet werden, dessen public key sowieso schon auf dem server unter authorized_keys hinterlegt ist, beispielsweise der secret key von tojio@devops vom devops-server)
- CICD_SERVER_USER
Benutzername auf dem remote server, zB admin
- CICD_SERVER
Name des remote servers, auf den das Projekt deployed werden soll, zB zasola.han-solo.net
- CICD_PROJECT_DIR
Projekt-Verzeichnis auf dem remote server als absoluter Pfad ohne abschließenden slash, zB /usr/local/www/apache24/data/tojio/ci-test
es muss sichergestellt werden, dass dieses Verzeichnis auf dem remote server existiert

Unter "CI / CD Settings | Runners | Available specific runners" muss der runner mit tag "devops-server-default" für das Projekt eingeschalten werden.

Im verwendenden php 8.0 Projekt wird in "CI / CD Settings | General pipelines | CI/CD configuration file" das file mit der gitlab-ci Konfiguration angegeben und damit auf dieses Projekt verwiesen: 
```sh
drupal/build-and-deploy.yml@tojio-public/gitlab-ci-common
```

Für Projekte, die php 8.2 verwenden:
```sh
drupal/build-and-deploy-82.yml@tojio-public/gitlab-ci-common
```

